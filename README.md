# Step project 2
<!-- TOC -->

  * [Автори](#автори)
  * [Використані тахнологіі](#використані-технологіі)
  * [Розподілення задач](#розподілення-задач) 
<!-- TOC -->

***
## Автори

* Поліна Шепель

* Аліна Хвіщук

***

## Використані технологіі
* HTML
* CSS
* SCSS
* Javascript
* Gulp
* Gitlab pages

***

## Розподілення задач

### Поліна Шепель
* Шапка сайту та верхнє меню
* Секція "People Are Talking About Fork"

### Аліна Хвіщук
* Секція "Revolutionary Editor"
* Секція "Here is what you get"
* Секція "Fork Subscription Pricing"

***
<!-- TOC -->
